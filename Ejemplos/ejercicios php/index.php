<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Práctica 17</title>
	<?php //include_once "libreria.php"; ?>
	<link rel="stylesheet" type="text/css" href="css/index.css">

</head>
<body>
	<div id="wrapper">
		<div class="header">
			<h1>Noticias</h1>
			<p>En esta web vemos las noticias de la base de datos.<br> Podemos introducir nuevas noticias.</p>
		</div>
		<div class="content">
			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>
				<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>			<div class="noticia">
				<div><h3>Título</h3></div>
				<div><p>Texto</p></div>
			</div>
		</div>
		<div class="footer">
			<h2></h2>
			<div class="tabla">
								<div class="cabecera">
									<div class="columna">#</div>
									<div class="celda">Título</div>
									<div class="celda">Texto</div>
									<div class="celda">Usuario</div>
								</div>
								<div class="fila">
									<div class="columna">1</div>
									<div class="celda">John</div>
									<div class="celda">Bob</div>
									<div class="celda">johnny81</div>
								</div>
								<div class="fila">
									<div class="columna">2</div>
									<div class="celda">Mary</div>
									<div class="celda">Brown</div>
									<div class="celda">missmary</div>
								</div>
								<div class="fila">
									<div class="columna">3</div>
									<div class="celda">James</div>
									<div class="celda">Mooray</div>
									<div class="celda">jijames</div>
								</div>
							</div>				
		</div>
	</div>
</body>
</html>